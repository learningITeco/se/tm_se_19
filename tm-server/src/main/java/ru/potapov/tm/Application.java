package ru.potapov.tm;

import org.jetbrains.annotations.NotNull;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import ru.potapov.tm.bootstrap.Bootstrap;

/**
 * Application
 * v 1.0.7
 */
import lombok.Getter;
import lombok.Setter;
import ru.potapov.tm.util.TransactionManagerFacoty;

import javax.enterprise.inject.se.SeContainerInitializer;

@Getter
@Setter
@ComponentScan("ru.potapov.tm")
public final class Application {
    @NotNull final private String version = "1.0.7";


    public Application() {
        try {
            ApplicationContext context = new AnnotationConfigApplicationContext(TransactionManagerFacoty.class);
            Bootstrap bootstrap = context.getBean(Bootstrap.class);
            bootstrap.init();
        }catch (Exception e){ e.printStackTrace(); }
    }

    public static void main(String[] args) {
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        new Application();
    }
}
