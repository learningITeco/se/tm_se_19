package ru.potapov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.AbstracEntity;

import javax.inject.Inject;
import java.lang.reflect.ParameterizedType;

@Getter
@Setter
@NoArgsConstructor
@Component
public abstract class AbstractServiceEndpoint<T extends AbstracEntity> {
    @Autowired @Nullable private ServiceLocator serviceLocator;
    @Nullable private String className = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName().toString().split("\\.")[((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName().toString().split("\\.").length-1];

    public AbstractServiceEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
