package ru.potapov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.dto.Session;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {
    @WebMethod boolean validSession(@WebParam @NotNull Session session) throws ValidateExeption;
    @WebMethod void addSession(@WebParam @NotNull Session session, String userId) throws ValidateExeption;
    @WebMethod Session getSessionById(@WebParam @NotNull String sessionId);
    @WebMethod void removeSession(@WebParam @NotNull Session session) throws ValidateExeption;
}
