package ru.potapov.tm.dto;

public enum RoleType {
    Administrator,
    User;
}
