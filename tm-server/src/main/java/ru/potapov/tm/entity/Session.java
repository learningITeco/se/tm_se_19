package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "app_session")
@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstracEntity implements Cloneable {

    @Id
    @Nullable
    private String id = UUID.randomUUID().toString();

    //@OneToOne(mappedBy="session", cascade=CascadeType.ALL)
    @OneToOne
    //@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="user_id")
    @Nullable private User user;

    @NotNull private String signature     = "";

    @Column(name = "timestamp")
    @NotNull private long dateStamp       = new Date().getTime();

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
