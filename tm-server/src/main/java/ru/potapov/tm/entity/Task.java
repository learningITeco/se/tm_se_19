package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "app_task")
@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstracEntity implements Cloneable, Serializable {

	@Id
	@Nullable private String id = UUID.randomUUID().toString();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="project_id")
	@Nullable private Project      project;

	@Column(name = "name", unique = true)
	@Nullable private String      name;

	@Column(name = "description")
	@Nullable private String      description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	@Nullable private User      user;

	@Column(name = "dateBegin")
	@Nullable private Date dateStart;

	@Column(name = "dateEnd")
	@Nullable private Date dateFinish;

	@Column(name = "status")
	@Enumerated(value = EnumType.STRING)
	@Nullable private Status status;

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
