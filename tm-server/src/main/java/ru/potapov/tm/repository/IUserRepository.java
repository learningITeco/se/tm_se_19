package ru.potapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.User;

@Repository
public interface IUserRepository extends CrudRepository<User, String> {
////    @Nullable
////    User findFirst (@NotNull final String login);
//
    @Nullable  User findOptionalByLogin (@NotNull final String login);
//
//    @Nullable User findById(@NotNull final String id);
//
//    @NotNull List<User> findAll();
//
//    User save(@NotNull User t);
//
//    void refresh(@NotNull User t);
//
//    void remove(@NotNull User t);
//
//    void removeAll();
}
