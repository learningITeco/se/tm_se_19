package ru.potapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Task;

@Repository
public interface ITaskRepository extends CrudRepository<Task, String> {

//    List<Task> findAll();
//
    Iterable<Task> findAllByUser(@NotNull final String userId);

    Iterable<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

//    Task findOptionalById(@NotNull String id);
//
    Task findByName(@NotNull String name);

//    Task findOptionalByNameAndUserId(@NotNull final String userId, @NotNull final String name);
//
//    void remove(@NotNull final Task task) ;
//
//    Task  save(@NotNull final Task task) ;
//
//    void refresh(@NotNull final Task task);
//
//    void removeAll(@NotNull  EntityManager em);
}
