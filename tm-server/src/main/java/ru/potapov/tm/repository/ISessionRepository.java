package ru.potapov.tm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Session;

@Repository
public interface ISessionRepository extends CrudRepository<Session, String> {
//
//    List<Session> findAll();
//
//    Session findById(@NotNull final String id);
//
//    Session findOptionalById(@NotNull final String id);
//
//    void refresh(@NotNull final Session session);
//
//    Session save(@NotNull final Session session) ;
//
//    void removeAll();
//
//    void remove(@NotNull Session session);
}
