import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.potapov.tm.endpoint.Session;
import ru.potapov.tm.endpoint.User;
import ru.potapov.tm.service.SessionService;
import ru.potapov.tm.service.UserService;

import java.util.Objects;

import static org.junit.Assert.*;

public class UserServiceTest {
    @Nullable private User currentUser;
    @NotNull private UserService userService;
    @NotNull private SessionService sessionService;

    @Before
    public void setUp() throws Exception {
        userService = new UserService();
    }

    @Test
    public void isAdministrator() {
        boolean isAdmin = false;

        User adminUser = null;
        Session session = null;

        try {
            adminUser = userService.getUserByNamePass("test", "1");
            if ( Objects.nonNull(adminUser) ){
                session = adminUser.getSession();
            }
            isAdmin = userService.getWebService().isAdministrator(session, adminUser);

        }catch (Exception e){e.printStackTrace();}

        Assert.assertTrue("admin is not admin", isAdmin);
    }

    @Test
    public void size() {
        int i = 0;
        try {
            i =  userService.size();
        }catch (Exception e){e.printStackTrace();}

        Assert.assertTrue("Size of users is 0", i > 0);
    }

    @Test
    public void getUserByNamePass() {
        User adminUser = null;
        Session session = null;
        try {
            adminUser =  userService.getUserByNamePass("test", "1");
        }catch (Exception e){e.printStackTrace();}

        Assert.assertNotNull("admin does not authorized", adminUser);
        session = adminUser.getSession();

        try {
            if (session != null)
                new SessionService().removeSession( session );
        }catch (Exception e){e.printStackTrace();}
    }

    @Test
    public void getUserByName() {
        User adminUser = null;
        Session session = null;
        try {
            adminUser =  userService.getUserByName("test");
        }catch (Exception e){e.printStackTrace();}

        Assert.assertNotNull("admin does not authorized", adminUser);
        try {
            if (session != null)
                new SessionService().removeSession( session );
        }catch (Exception e){e.printStackTrace();}
    }

}