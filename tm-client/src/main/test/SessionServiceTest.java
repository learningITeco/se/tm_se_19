import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.potapov.tm.endpoint.Session;
import ru.potapov.tm.endpoint.User;
import ru.potapov.tm.service.SessionService;
import ru.potapov.tm.service.UserService;

import static org.junit.Assert.*;

public class SessionServiceTest {
    @Nullable private User currentUser;
    @Nullable private Session currentSession;
    @NotNull private UserService userService;
    @NotNull private SessionService sessionService;

    @Before
    public void setUp() throws Exception {
        userService = new UserService();
        sessionService = new SessionService();

        //User & session
        User adminUser = null;
        Session session = null;
        try {
            adminUser =  userService.getUserByNamePass("test", "1");
        }catch (Exception e){e.printStackTrace();}

        session = adminUser.getSession();
        sessionService.setSession(session);
        userService.setAuthorizedUser(adminUser);
        userService.setAuthorized(true);
        currentSession = session;
    }

    @Test
    public void removeSession() {
        if (currentSession != null)
            sessionService.removeSession( currentSession );
        Session s = sessionService.getSessionById(currentSession.getId());
        Assert.assertNull(s);
    }
}