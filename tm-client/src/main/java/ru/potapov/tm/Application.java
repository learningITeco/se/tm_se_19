package ru.potapov.tm;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.potapov.tm.bootstrap.Bootstrap;

@Getter
@Setter
public final class Application {
    @NotNull final private String version = "1.0.19";


    public Application() {
        try {
            ApplicationContext context = new AnnotationConfigApplicationContext(Bootstrap.class);
            Bootstrap bootstrap = context.getBean(Bootstrap.class);
            bootstrap.init();
        }catch (Exception e){ e.printStackTrace(); }
    }

    public static void main(String[] args) {
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        new Application();
    }
}
